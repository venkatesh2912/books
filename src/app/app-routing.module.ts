import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { bookRoute } from  './books';

const routes: Routes = [
  ...bookRoute,
  {
    path: '',
    redirectTo: 'books',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
