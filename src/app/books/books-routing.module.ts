import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewComponent } from './view/view.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import  {childRoute} from './'
const routes: Routes = [
  {
    path: '',
    component: ViewComponent,
  },
  {
    path: 'add',
    component: AddComponent,
    children:[
      ...childRoute
    ]
  },
  {
    path: 'edit',
    component: EditComponent,
    children:[
      ...childRoute
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BooksRoutingModule { }
