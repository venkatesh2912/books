import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit',
  templateUrl: './../add/add.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  title :string;
  constructor() { }

  ngOnInit() 
  {
    this.title = ' Edit Books Management';
  }

}
