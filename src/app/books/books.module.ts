import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormWizardModule } from 'angular2-wizard';
import { FormsModule }        from '@angular/forms';

import { BooksRoutingModule } from './books-routing.module';
import { ViewComponent } from './view/view.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { NavbarComponent }    from './navbar/navbar.component';
import { FormDataService }    from './data/formData.service';
import { PersonalComponent }  from './personal/personal.component';
import { WorkflowService }    from './workflow/workflow.service';
import { WorkComponent }      from './work/work.component';
import { AddressComponent }   from './address/address.component';
import { ResultComponent }    from './result/result.component';

@NgModule({
  imports: [
    CommonModule,
    BooksRoutingModule,
    FormWizardModule,
    FormsModule
  ],
  providers:    [{ provide: FormDataService, useClass: FormDataService },
    { provide: WorkflowService, useClass: WorkflowService }],
  declarations: [ViewComponent, AddComponent, EditComponent,NavbarComponent,PersonalComponent,WorkComponent,AddressComponent,ResultComponent]
})
export class BooksModule { }
