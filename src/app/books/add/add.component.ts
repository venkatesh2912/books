import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormDataService }            from './../data/formData.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  title :string;
    @Input() formData;
    
    constructor(private formDataService: FormDataService) {
    }

    ngOnInit() {
        this.formData = this.formDataService.getFormData();
        console.log(this.title + ' loaded!');
        this.title = ' Add Books Management';
    }
}
