
import { PersonalComponent }  from './personal/personal.component';
import { WorkComponent }      from './work/work.component';
import { AddressComponent }   from './address/address.component';
import { ResultComponent }    from './result/result.component';
import { WorkflowGuard }        from './workflow/workflow-guard.service';

export var bookRoute = [
	{
        path: 'books',
        loadChildren: './books/books.module#BooksModule'
    }
];
export var childRoute = [
    { path: 'personal',  component: PersonalComponent },
      { path: 'work',  component: WorkComponent },
      { path: 'address',  component: AddressComponent },
      { path: 'result',  component: ResultComponent },
      { path: '',   redirectTo: 'personal', pathMatch: 'full' },
      { path: '**', component: PersonalComponent }
]